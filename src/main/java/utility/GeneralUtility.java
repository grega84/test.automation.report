package utility;

import java.util.Map;

import org.apache.commons.lang3.text.StrSubstitutor;

public class GeneralUtility {
	
	public static final String stingFilter(String template, Map<String, String> map) {
		StrSubstitutor sub = new StrSubstitutor(map);
		return sub.replace(template);
	}
}
