package utility;

public final class TemplateQuery {
	public static final String QUERY_GET_PROJECT_BY_NAME="FROM Project WHERE NAME='${PROJECT_NAME}'";
	public static final String QUERY_GET_VERSION_BY_DATA="FROM Version WHERE NAME='${VERSION_NAME}' And ID_PROJECT='${ID_PROJECT}' and sw_version='${SW_VERSION}'";
	public static final String QUERY_GET_CYCLE_FEATURE_BY_DATA="FROM CycleFeatures WHERE CYCLE_NAME='${CYCLE_NAME}'";
	public static final String QUERY_GET_CYCLE_PER_VERSION_BY_DATA="FROM CyclePerVersion Where id_version='${ID_VERSION}' and id_cycle='${ID_CYCLE}'";
	public static final String QUERY_GET_STEPS_CUCUMBER_STEPS_BY_DATA="FROM StepsCucumberSteps WHERE cucumber_keyword ='${CUCUMBER_KEYWORD}' AND NAME='${NAME}'";
	public static final String QUERY_GET_TEST_SCENARIO_BY_NAME="FROM TestScenario WHERE NAME='${TEST_NAME}' and id_jira='${JIRA_ID}'";
	public static final String QUERY_GET_TEST_STEP_BY_ID="FROM TestStep WHERE id_Test='${ID_TEST}' and id_step='${ID_STEP}'";
	public static final String QUERY_GET_TEST_EXECUTION_BY_IDS="FROM TestExecution WHERE id_project='${ID_PROJECT}' and id_version='${ID_VERSION}' AND ID_CYCLE='${ID_CYCLE}' AND ID_TEST='${ID_TEST}' AND count_outline=${COUNTER_OUTLINE} and id_device=${ID_DEVICE}";
	public static final String QUERY_GET_TEST_EXECUTION_BY_IDS_COUNTOUTLINE="FROM TestExecution WHERE id_project='${ID_PROJECT}' and id_version='${ID_VERSION}' AND ID_CYCLE='${ID_CYCLE}' AND ID_TEST='${ID_TEST}' and id_device=${ID_DEVICE}";
	public static final String QUERY_GET_DEVICE_BY_DATA="FROM Device where name='${DEVICE_NAME}' and brand='${DEVICE_BRAND}'and version='${DEVICE_VERSION}'";
	public static final String QUERY_GET_TEST_EXECUTION_DATA="FROM TestExecutionData WHERE id_step_execution='${STEP_EXECITUION_ID}'";
	public static final String QUERY_GET_VIDEO_BY_DATA = "from Video where video_name='${VIDEO_NAME}'";
	public static final String QUERY_GET_TEST_STEPS_BY_DATA = "from TestSteps where id_test='${ID_TEST}' and id_step='${ID_STEP}'";
	public static final String QUERY_GET_TEST_DATAEXECUTION_BY_IDS = "from TestExecutionData where id_step_execution=${ID_TEST_EXECUTION} and data_key='${DATA_KEY}' and value='${DATA_VALUE}'";
	public static final String QUERY_GET_TEST_STEPS_EXECUTION_BY_DATA = "from TestExecutionStep where id_test_execution=${ID_TEST_EXECUTION} and id_step=${ID_STEP}";
	public static final String QUERY_GET_CYCLE_REPORT_BY_DATA = "from CycleExecutionReport where id_cycle_version=${ID_CYCLE_VERSION}";
	public static final String QUERY_GET_TOTAL_PROJECTS_RUN_BY_DATA = "from TotalProjectRuns where id_project=${PROJECT_ID}";
}
