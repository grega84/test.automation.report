package ftp;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;

public class FtpFileTransferFactory 
{
	private static final String FTP_HOST = "ftp.host";
	private static final String DEFAULT_FTP_PROPERTIES_BUNDLE_KEY = "default";
	private static FtpFileTransferFactory singleton;
	private HashMap<String, FtpFileTransfer> context;
	private Map<String, Properties> propertiesBundles = null;
	
	private FtpFileTransferFactory() throws Exception 
	{
		// load properties
		context = new HashMap<String, FtpFileTransfer>();
		/*
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);
		propertiesBundles = multiPropertiesLoader.loadAndCheck((d, name) -> name.endsWith("-ftp.properties"),
				FTP_HOST);
		
		if (!propertiesBundles.containsKey(DEFAULT_FTP_PROPERTIES_BUNDLE_KEY)) 
		{
			throw new Exception("No default-sftp.properties found in the "
					.concat(((FileMultiPropertiesLoaderImpl) multiPropertiesLoader).getPath()));
		}
		
		propertiesBundles.forEach((k, v) -> context.put(k, initTemplate(v)));*/
		Properties prop=new Properties();
		InputStream i=FtpFileTransferFactory.class.getClassLoader().getResourceAsStream("default-ftp.properties");
		prop.load(i);
		
		context.put(DEFAULT_FTP_PROPERTIES_BUNDLE_KEY, initTemplate(prop));
	}

	private FtpFileTransfer initTemplate(Properties v) 
	{
		return new FtpFileTransfer(v);
	}
	
	public synchronized static FtpFileTransferFactory getInstance() throws Exception
	{
		if(singleton == null)
			singleton=new FtpFileTransferFactory();
		
		return singleton;
	}
	
	public FtpFileTransfer get()
	{
		return context.get(DEFAULT_FTP_PROPERTIES_BUNDLE_KEY);
	}
	
	public FtpFileTransfer get(String name) throws Exception
	{
		if(!context.containsKey(name))
			throw new Exception("the configuration file "+name+"-ftp.properties do not exists");
		
		return context.get(name);
	}
}
