package ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.springframework.util.Assert;


public class FtpFileTransfer 
{
	private static final String HOST = "ftp.host";
	private static final String USERNAME = "ftp.username";
	private static final String PASSWORD = "ftp.password";
	private static final String PORT = "ftp.port";
	private static final String SSL = "ftp.ssl";
	private static final int DEFAULT_PORT = 21;
	private static final int DEFAULT_SSL_PORT = 22;
	
	private Properties properties;
	FTPClient ftpClient;
	
	public FtpFileTransfer(Properties v) 
	{
		Assert.notNull(v, "Properties have not been set!");
		Assert.notNull(v.getProperty(SSL),SSL+" parameter is mandatory");
		Assert.notNull(v.getProperty(HOST),HOST+" parameter is mandatory");
		Assert.notNull(v.getProperty(USERNAME),USERNAME+" parameter is mandatory");
		Assert.notNull(v.getProperty(PASSWORD),PASSWORD+" parameter is mandatory");
		
		this.properties = v;
		
		if(v.getProperty(SSL).equals("true"))
			ftpClient=new FTPSClient(true);
		else
			ftpClient=new FTPClient();
		
	}
	
	public List<String> getFileList(String remoteDir) throws Exception
	{
		ArrayList<String> files=new ArrayList<>();
		
		try {
			createSession();
			boolean success = ftpClient.changeWorkingDirectory(remoteDir);
			
			if(!success)
				throw new Exception("Error occurs on access to folder "+remoteDir);
			
			FTPFile[] F=ftpClient.listFiles();
			
			for(FTPFile f : F)
			{
				files.add(f.getName());
			}
			
		} catch (Exception e) 
		{
			closeSession();
			throw e;
		}
		finally
		{
			closeSession();
		}
		
		
		return files;
	}

	private void closeSession() throws Exception 
	{
		// logs out
        ftpClient.logout();
        ftpClient.disconnect();
	}

	private void createSession() throws Exception 
	{
		if(!checkMandatory())
		{
			throw new Exception("mandatory fields are not properly setted");
		}
		
		int port=(properties.containsKey(PORT)) ? 
				Integer.parseInt(properties.getProperty(PORT)) 
				: (properties.getProperty(SSL).equals("true")) ? DEFAULT_SSL_PORT : DEFAULT_PORT;
		
		ftpClient.connect(properties.getProperty(HOST), port);
		
        if(!ftpServerReply200())
        {
        	throw new Exception("Error response from ftp server "+ftpClient.getReplyString());
        }
        
        boolean success = ftpClient.login(properties.getProperty(USERNAME), properties.getProperty(PASSWORD));
        
        if(!success)
        {
        	throw new Exception("connection refused for credential:"+properties.getProperty(USERNAME)+":"+properties.getProperty(PASSWORD));
        }
        
        ftpClient.enterLocalPassiveMode();
	}

	private boolean ftpServerReply200() 
	{
		return FTPReply.isPositiveCompletion(ftpClient.getReplyCode());
	}

	private boolean checkMandatory() 
	{
		if(properties.get(HOST) == null)
			return false;
		if(properties.get(USERNAME) == null)
			return false;
		if(properties.get(PASSWORD) == null)
			return false;
		if(properties.get(SSL) == null)
			return false;
		
		return true;
	}

	public void upload(String localPath, String remotePath) throws Exception 
	{
		try {
			createSession();
			
			File firstLocalFile = new File(localPath);
			InputStream inputStream = new FileInputStream(firstLocalFile);
			System.out.println("Start uploading file");
			
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			
			OutputStream outputStream = ftpClient.storeFileStream(remotePath);
            byte[] bytesIn = new byte[4096];
            int read = 0;
 
            while ((read = inputStream.read(bytesIn)) != -1) {
                outputStream.write(bytesIn, 0, read);
            }
            inputStream.close();
            outputStream.close();
 
            boolean done = ftpClient.completePendingCommand();
            
            if (done) {
                System.out.println("The file is uploaded successfully.");
            }
            else
            {
            	throw new Exception("Error occurs during the upload of file "+localPath+" to "+remotePath);
            }
			
		} catch (Exception e) 
		{
			closeSession();
			throw e;
		}
		finally
		{
			closeSession();
		}
		
	}

}
