package db.manager;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.internal.SessionImpl;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;

import db.pojo.CycleFeatures;
import db.pojo.Project;
import db.pojo.Version;

public class Manager 
{
	private static final Logger log=LogManager.getLogger(Manager.class);
	
	SessionFactory config;
	
	public Manager() 
	{
		System.out.println("config file:"+Manager.class.getClassLoader().getResource("conf/hibernate.cfg.xml"));
		log.info("config file:"+new File(Manager.class.getClassLoader().getResource("conf/hibernate.cfg.xml").getPath()));
		config=new Configuration().configure().buildSessionFactory();
		
	}
	
	public static void main(String[] args) {
		Manager man= new Manager();
		Project project= new Project();
		project.setName("Bancoposta App");
		project.setDescription("description");
		project.setImage("lll");

		project=(Project)man.populateOrGetTable(project, "FROM Project WHERE NAME='"+project.getName()+"'");
		Version version= new Version();
		version.setIdProject(project.getId());

	}

	public Object populateOrGetTable(Object objectTable,String pQueryText) {
		Session session=config.getCurrentSession();
		try {
			session.beginTransaction();
			Query query= session.createQuery(pQueryText);
			List<Object> result= query.list();
			if(result.size()==0) {
				session.save(objectTable);
				session.getTransaction().commit();
				log.info("-non esiste----------------"+ objectTable.toString());
			}else {
				objectTable=result.get(0);
				log.info("-esiste gi�----------------"+ objectTable.toString());
			}
			log.info("-endif----------------"+ objectTable.toString());
			session.close();
		} catch(Exception err) {
			System.out.println(">>>>>>>>>>> ERRORE UPDATE DATABASE <<<<<<<<<<<<<<");
			err.printStackTrace();
			if(session.getTransaction() != null)
				session.getTransaction().rollback();
			session.close();
			objectTable=null;
		}
		finally {
			session.close();
		}
		return objectTable;
	}
	public boolean tableIdExists(Object object,String pQueryText) {
		Session session=config.getCurrentSession();
		List<Object> result=new ArrayList<Object>();
		try {
			session.beginTransaction();
			Query query= session.createQuery(pQueryText);
			result= query.list();
			session.close();
		}catch(Exception err)
		{
			System.out.println(">>>>>>>>>>>>>>>>>> ERRORE UPDATE DATABASE <<<<<<<<<<<<<<");
			err.printStackTrace();
			if(session.getTransaction() != null)
				session.getTransaction().rollback();
			session.close();
		} finally {
			session.close();
		}
		return !result.isEmpty();
	}
	
	public List<Object> getElementFromTable(String pQueryText)
	{
		Session session=config.getCurrentSession();
		Object o=null;
		try
		{
			session.beginTransaction();
			Query query= session.createQuery(pQueryText);
			List<Object> result=new ArrayList<Object>();
			result=query.list();
			session.close();
			
			if(!result.isEmpty())
				return result;
			else
				return null;
			
		}catch(Exception err)
		{
			System.out.println(">>>>>>>>>>>>>>>>>> ERRORE UPDATE DATABASE <<<<<<<<<<<<<<");
			err.printStackTrace();
			if(session.getTransaction() != null)
				session.getTransaction().rollback();
			session.close();
			
			return null;
		}
		finally
		{
			session.close();
		}
	}
	
	public Object populateTable(Object objectTable) {
		Session session=config.getCurrentSession();
		try {
			session.beginTransaction();
				session.save(objectTable);
				session.getTransaction().commit();
				session.close();
		}
		catch(Exception err)
		{
			System.out.println(">>>>>>>>>>>>>>>>>> ERRORE UPDATE DATABASE <<<<<<<<<<<<<<");
			err.printStackTrace();
			if(session.getTransaction() != null)
				session.getTransaction().rollback();
			session.close();
		}
		finally {
			session.close();
		}
		return objectTable;
	}
	
	public Object updateTable(Object objectTable)
	{
		Session session=config.getCurrentSession();
		try {
			session.beginTransaction();
			session.update(objectTable);
			session.getTransaction().commit();
			session.close();
		}catch(Exception err)
		{
			System.out.println(">>>>>>>>>>>>>>>>>> ERRORE UPDATE DATABASE <<<<<<<<<<<<<<");
			err.printStackTrace();
			if(session.getTransaction() != null)
				session.getTransaction().rollback();
			session.close();
		} finally {
			session.close();
		}
		return objectTable;
	}
	
	public void callProcedure(String proc)
	{
		Session session=config.getCurrentSession();
		try
		{
			session.beginTransaction();
			
			Query query = session.createSQLQuery("CALL "+proc);
			
			query.list();
			//session.getTransaction().commit();
			session.close();
		}
		catch(Exception err)
		{
			System.out.println(">>>>>>>>>>>>>>>>>> ERRORE UPDATE DATABASE <<<<<<<<<<<<<<");
			err.printStackTrace();
			if(session.getTransaction() != null)
				session.getTransaction().rollback();
			session.close();
		}
		finally
		{
			session.close();
		}
	}

	public ResultSet queryFor(String sql) 
	{
		Session session=config.getCurrentSession();
		try {
			ResultSet rs=null;
			session.beginTransaction();
			
			Connection conn=config.
	                getSessionFactoryOptions().getServiceRegistry().
	                getService(ConnectionProvider.class).getConnection();
			
			rs=conn.prepareStatement(sql).executeQuery();
			
			session.close();
			
			return rs;
		} catch (Exception e) {
			System.out.println(">>>>>>>>>>>>>>>>>> ERRORE UPDATE DATABASE <<<<<<<<<<<<<<");
			e.printStackTrace();
			if(session.getTransaction() != null)
				session.getTransaction().rollback();
			session.close();
			return null;
		}finally {
			session.close();
		}
	}

}
