package db.pojo;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "test_execution")
public class TestExecution {
	public TestExecution() {
		this.countOutline=0;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_step_execution")
	private long idStepExecution;
	@Column(name="count_outline")
	private int countOutline;
	@Column(name="id_project")
	private int idProject;
	@Column(name="id_version")
	private int idVersion;
	@Column(name="id_test")
	private int idTest;
	@Column(name="id_device")
	private int idDevice;
	@Column(name="execution_date")
	private Timestamp executionDate;
	@Column(name="result")
	private String result;
	@Column(name="error_log")
	private String errorLog;
	@Column(name="video_id")
	private int videoId;
	@Column(name="execution_duration")
	private String executionDuration;
	@Column(name="id_cycle")
	private int idCycle;
	@Column(name="result_automa")
	private String resultAutoma;
	@Column(name="validation_reason")
	private String validationReason;
	@Column(name="to_validate")
	private int toValidate;
	
	
	
	public String getResultAutoma() {
		return resultAutoma;
	}
	public void setResultAutoma(String resultAutoma) {
		this.resultAutoma = resultAutoma;
	}
	public String getValidationReason() {
		return validationReason;
	}
	public void setValidationReason(String validationReason) {
		this.validationReason = validationReason;
	}
	public int getToValidate() {
		return toValidate;
	}
	public void setToValidate(int toValidate) {
		this.toValidate = toValidate;
	}
	public int getVideoId() {
		return videoId;
	}
	public void setVideoId(int videoId) {
		this.videoId = videoId;
	}
	public int getCountOutline() {
		return countOutline;
	}
	public void setCountOutline(int countOutline) {
		this.countOutline = countOutline;
	}
	
	public int getIdCycle() {
		return idCycle;
	}
	public void setIdCycle(int idCycle) {
		this.idCycle = idCycle;
	}
	public long getIdStepExecution() {
		return idStepExecution;
	}
	public void setIdStepExecution(long idStepExecution) {
		this.idStepExecution = idStepExecution;
	}
	public int getIdProject() {
		return idProject;
	}
	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	public int getIdVersion() {
		return idVersion;
	}
	public void setIdVersion(int idVersion) {
		this.idVersion = idVersion;
	}
	public int getIdTest() {
		return idTest;
	}
	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}
	
	public int getIdDevice() {
		return idDevice;
	}
	public void setIdDevice(int idDevice) {
		this.idDevice = idDevice;
	}
	public Timestamp getExecutionDate() {
		return executionDate;
	}
	public void setExecutionDate(Timestamp executionDate) {
		this.executionDate = executionDate;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getErrorLog() {
		return errorLog;
	}
	public void setErrorLog(String errorLog) {
		this.errorLog = errorLog;
	}
	
	public String getExecutionDuration() {
		return executionDuration;
	}
	public void setExecutionDuration(String executionDuration) {
		this.executionDuration = executionDuration;
	}
	public TestExecution(long idStepExecution, int countOutline, int idProject, int idVersion, int idTest,
			int idDevice, Timestamp executionDate, String result, String errorLog, int videoId,
			String executionDuration, int idCycle) {
		super();
		this.countOutline = countOutline;
		this.idProject = idProject;
		this.idVersion = idVersion;
		this.idTest = idTest;
		
		this.idDevice = idDevice;
		this.executionDate = executionDate;
		this.result = result;
		this.errorLog = errorLog;
		this.videoId = videoId;
		this.executionDuration = executionDuration;
		this.idCycle = idCycle;
	}
	@Override
	public String toString() {
		return "TestExecution [idStepExecution=" + idStepExecution + ", countOutline=" + countOutline + ", idProject="
				+ idProject + ", idVersion=" + idVersion + ", idTest=" + idTest + ", idDevice="
				+ idDevice + ", executionDate=" + executionDate + ", result=" + result + ", errorLog=" + errorLog
				+ ", videoId=" + videoId + ", executionDuration=" + executionDuration + ", idCycle=" + idCycle + "]";
	}
	
			
}
