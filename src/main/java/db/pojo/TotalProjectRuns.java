package db.pojo;

import javax.persistence.Column;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "total_project_runs")
public class TotalProjectRuns 
{
	@Id
	@Column(name="id_project")
	private int idProject;
	@Column(name="total_passed")
	private int totalPassed;
	@Column(name="total_failed")
	private int totalFailed;
	@Column(name="total_runs")
	private int totalRuns;
	@Column(name="devices_id_list")
	private String devicesIdList;
	public int getIdProject() {
		return idProject;
	}
	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	public int getTotalPassed() {
		return totalPassed;
	}
	public void setTotalPassed(int totalPassed) {
		this.totalPassed = totalPassed;
	}
	public int getTotalFailed() {
		return totalFailed;
	}
	public void setTotalFailed(int totalFailed) {
		this.totalFailed = totalFailed;
	}
	public int getTotalRuns() {
		return totalRuns;
	}
	public void setTotalRuns(int totalRuns) {
		this.totalRuns = totalRuns;
	}
	public String getDevicesIdList() {
		return devicesIdList;
	}
	public void setDevicesIdList(String devicesIdList) {
		this.devicesIdList = devicesIdList;
	}
	public TotalProjectRuns(int idProject, int totalPassed, int totalFailed, int totalRuns, String devicesIdList) {
		super();
		this.idProject = idProject;
		this.totalPassed = totalPassed;
		this.totalFailed = totalFailed;
		this.totalRuns = totalRuns;
		this.devicesIdList = devicesIdList;
	}
	public TotalProjectRuns() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "TotalProjectRuns [idProject=" + idProject + ", totalPassed=" + totalPassed + ", totalFailed="
				+ totalFailed + ", totalRuns=" + totalRuns + ", devicesIdList=" + devicesIdList + "]";
	}
	

}
