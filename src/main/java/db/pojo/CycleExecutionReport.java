package db.pojo;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@javax.persistence.Entity
@javax.persistence.Table(name = "cycle_execution_report")
public class CycleExecutionReport {

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cycle_version")
	private int idCycleVersion;
	@Column(name="total_passed")
	private int totalPassed;
	@Column(name="total_failed")
	private int totalFailed;
	@Column(name="result")
	private String result;
	@Column(name="total_execution_time")
	private String totalExecutionTime;
	public int getIdCycleVersion() {
		return idCycleVersion;
	}
	public void setIdCycleVersion(int idCycleVersion) {
		this.idCycleVersion = idCycleVersion;
	}
	public int getTotalPassed() {
		return totalPassed;
	}
	public void setTotalPassed(int totalPassed) {
		this.totalPassed = totalPassed;
	}
	public int getTotalFailed() {
		return totalFailed;
	}
	public void setTotalFailed(int totalFailed) {
		this.totalFailed = totalFailed;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getTotalExecutionTime() {
		return totalExecutionTime;
	}
	public void setTotalExecutionTime(String totalExecutionTime) {
		this.totalExecutionTime = totalExecutionTime;
	}
	
	public CycleExecutionReport(int totalPassed, int totalFailed, String result, String totalExecutionTime) {
		this.totalPassed = totalPassed;
		this.totalFailed = totalFailed;
		this.result = result;
		this.totalExecutionTime = totalExecutionTime;
	}
	
	public CycleExecutionReport(){}
	

}
