package db.pojo;

import javax.persistence.Column;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "text_execution_data")
public class TestExecutionData {
	public TestExecutionData() {
	}
	@Id
	@Column(name="id_step_execution")
	private long idStepExecution;
	@Column(name="data_key")
	private String dataKey;
	@Column(name="value")
	private String value;
	public long getIdStepExecution() {
		return idStepExecution;
	}
	public void setIdStepExecution(long idStepExecution) {
		this.idStepExecution = idStepExecution;
	}
	public String getDataKey() {
		return dataKey;
	}
	public void setDataKey(String dataKey) {
		this.dataKey = dataKey;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public TestExecutionData(String dataKey, String value) {
		this.dataKey = dataKey;
		this.value = value;
	}
	@Override
	public String toString() {
		return "TestExecutionData [idStepExecution=" + idStepExecution + ", dataKey=" + dataKey + ", value=" + value
				+ "]";
	}
	

}
