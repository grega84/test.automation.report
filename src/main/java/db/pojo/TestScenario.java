package db.pojo;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "test_scenario")
public class TestScenario {
	public TestScenario() {
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_test")
	private int idTest;
	@Column(name="name")
	private String name;
	@Column(name="description")
	private String description;
	@Column(name="id_jira")
	private String idJira;
	public int getIdTest() {
		return idTest;
	}
	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIdJira() {
		return idJira;
	}
	public void setIdJira(String idJira) {
		this.idJira = idJira;
	}
	public TestScenario( String name, String description, String idJira) {
		super();
		this.name = name;
		this.description = description;
	}
	@Override
	public String toString() {
		return "TestScenario [idTest=" + idTest + ", name=" + name + ", description=" + description + ", idJira="
				+ idJira + "]";
	}
	

}
