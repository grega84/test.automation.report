package db.pojo;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "device")
public class Device {
	public Device() {
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_device")
	private int idDevice;
	@Column(name="name")
	private String name;
	@Column(name="os")
	private String os;
	@Column(name="version")
	private String version;
	@Column(name="brand")
	private String brand;
	@Column(name="screen_size")
	private String screenSize;
	@Column(name="resolution")
	private String resolution;
	@Column(name="category")
	private String category;
	@Column(name="device_image")
	private String deviceImage;
	@Column(name="icon")
	private String icon;
	
	
	
	public String getDeviceImage() {
		return deviceImage;
	}
	public void setDeviceImage(String deviceImage) {
		this.deviceImage = deviceImage;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public int getIdDevice() {
		return idDevice;
	}
	public void setIdDevice(int idDevice) {
		this.idDevice = idDevice;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getScreenSize() {
		return screenSize;
	}
	public void setScreenSize(String screenSize) {
		this.screenSize = screenSize;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	

}
