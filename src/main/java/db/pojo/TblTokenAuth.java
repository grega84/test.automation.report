package db.pojo;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "tbl_token_auth")
public class TblTokenAuth {
	public TblTokenAuth() {
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_row")
	private int idRow;
	@Column(name="auth0_hash")
	private String auth0Hash;
	@Column(name="auth1_hash")
	private String auth1Hash;
	@Column(name="secure_hash")
	private String secureHash;
	@Column(name="user_id")
	private String userId;
	@Column(name="expiration_date")
	private Date expirationDate;
	public int getIdRow() {
		return idRow;
	}
	public void setIdRow(int idRow) {
		this.idRow = idRow;
	}
	public String getAuth0Hash() {
		return auth0Hash;
	}
	public void setAuth0Hash(String auth0Hash) {
		this.auth0Hash = auth0Hash;
	}
	public String getAuth1Hash() {
		return auth1Hash;
	}
	public void setAuth1Hash(String auth1Hash) {
		this.auth1Hash = auth1Hash;
	}
	public String getSecureHash() {
		return secureHash;
	}
	public void setSecureHash(String secureHash) {
		this.secureHash = secureHash;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public TblTokenAuth(String auth0Hash, String auth1Hash, String secureHash, String userId, Date expirationDate) {
		super();
		this.auth0Hash = auth0Hash;
		this.auth1Hash = auth1Hash;
		this.secureHash = secureHash;
		this.userId = userId;
		this.expirationDate = expirationDate;
	}
	@Override
	public String toString() {
		return "TblTokenAuth [idRow=" + idRow + ", auth0Hash=" + auth0Hash + ", auth1Hash=" + auth1Hash
				+ ", secureHash=" + secureHash + ", userId=" + userId + ", expirationDate=" + expirationDate + "]";
	}
	
}
