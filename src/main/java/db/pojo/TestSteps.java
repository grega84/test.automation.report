package db.pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "test_steps")
public class TestSteps implements Serializable
{
	
	@Id
	@Column(name="id_test")
	private int idTest;
	@Id
	@Column(name="id_step")
	private int idStep;
	public int getIdTest() {
		return idTest;
	}
	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}
	public int getIdStep() {
		return idStep;
	}
	public void setIdStep(int idStep) {
		this.idStep = idStep;
	}
	
	
}
