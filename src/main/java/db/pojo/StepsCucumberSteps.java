package db.pojo;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "Steps_cucumber_steps")
public class StepsCucumberSteps {
	public StepsCucumberSteps() {
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="cucumber_keyword")
	private String cucumberKeyword;
	@Column(name="name")
	private String name;
	public StepsCucumberSteps(String cucumberKeyword, String name) {
		super();
		this.cucumberKeyword = cucumberKeyword;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCucumberKeyword() {
		return cucumberKeyword;
	}
	public void setCucumberKeyword(String cucumberKeyword) {
		this.cucumberKeyword = cucumberKeyword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "StepsCucumberSteps [id=" + id + ", cucumberKeyword=" + cucumberKeyword + ", name=" + name + "]";
	}
	
}
