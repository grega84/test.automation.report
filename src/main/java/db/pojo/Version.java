package db.pojo;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "version")
public class Version {
	public Version() {
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_version")
	private int idVersion;
	@Column(name="id_project")
	private int idProject;
	@Column(name="name")
	private String name;
	@Column(name="start_date")
	private Timestamp startDate;
	@Column(name="end_date")
	private Timestamp endDate;
	@Column(name="status")
	private String status;
	@Column(name="sw_version")
	private String swVersion;
	
	public Version(int idProject, String name, Timestamp startDate, Timestamp endDate, String status, String swVersion) {
		super();
		this.idProject = idProject;
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
		this.swVersion=swVersion;
	}
	
	
	public String getSwVersion() {
		return swVersion;
	}


	public void setSwVersion(String swVersion) {
		this.swVersion = swVersion;
	}


	public int getIdVersion() {
		return idVersion;
	}
	public void setIdVersion(int idVersion) {
		this.idVersion = idVersion;
	}
	public int getIdProject() {
		return idProject;
	}
	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Version [idVersion=" + idVersion + ", idProject=" + idProject + ", name=" + name + ", startDate="
				+ startDate + ", endDate=" + endDate + ", status=" + status + ", swVersion="+swVersion+"]";
	}
	

}
