package db.pojo;

import javax.persistence.Column;


@javax.persistence.Entity
@javax.persistence.Table(name = "project")
public class ProjectGroup {
	public ProjectGroup() {
	}
	@Column(name="id_project")
	private int idProject;
	@Column(name="id_group")
	private String idGroup;
	public int getIdProject() {
		return idProject;
	}
	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	public String getIdGroup() {
		return idGroup;
	}
	public void setIdGroup(String idGroup) {
		this.idGroup = idGroup;
	}
	public ProjectGroup(int idProject, String idGroup) {
		super();
		this.idProject = idProject;
		this.idGroup = idGroup;
	}
	@Override
	public String toString() {
		return "ProjectGroup [idProject=" + idProject + ", idGroup=" + idGroup + "]";
	}


}
