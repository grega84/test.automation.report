package db.pojo;

import javax.persistence.Column;


@javax.persistence.Entity
@javax.persistence.Table(name = "user_roles")
public class UserRoles {
	public UserRoles() {
	}
	@Column(name="user_id")
	private int user_id;
	@Column(name="role_id")
	private int role_id;
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public UserRoles(int user_id, int role_id) {
		super();
		this.user_id = user_id;
		this.role_id = role_id;
	}
	@Override
	public String toString() {
		return "UserRoles [user_id=" + user_id + ", role_id=" + role_id + "]";
	}
	
}
