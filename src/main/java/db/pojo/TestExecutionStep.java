package db.pojo;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "test_execution_step")
public class TestExecutionStep 
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_step_execution")
	private long idStepExecution;
	@Column(name="id_test_execution")
	private long idTestExecution;
	@Column(name="id_step")
	private int idStep;
	@Column(name="result")
	private String result;
	@Column(name="execution_duration")
	private String executionDuration;
	
	
	public long getIdStepExecution() {
		return idStepExecution;
	}
	public void setIdStepExecution(long idStepExecution) {
		this.idStepExecution = idStepExecution;
	}
	public long getIdTestExecution() {
		return idTestExecution;
	}
	public void setIdTestExecution(long idTestExecution) {
		this.idTestExecution = idTestExecution;
	}
	public int getIdStep() {
		return idStep;
	}
	public void setIdStep(int idStep) {
		this.idStep = idStep;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getExecutionDuration() {
		return executionDuration;
	}
	public void setExecutionDuration(String executionDuration) {
		this.executionDuration = executionDuration;
	}
	
	
}
