package db.pojo;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "user_group")
public class UserGroup {
	public UserGroup() {
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private int user_id;
	@Column(name="group_id")
	private int group_id;
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public UserGroup(int user_id, int group_id) {
		super();
		this.user_id = user_id;
		this.group_id = group_id;
	}
	@Override
	public String toString() {
		return "UserGroup [user_id=" + user_id + ", group_id=" + group_id + "]";
	}
	
}
