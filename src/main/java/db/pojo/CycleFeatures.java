package db.pojo;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "cycle_features")
public class CycleFeatures {
	public CycleFeatures() {}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="cycle_name")
	private String cycleName;
	@Column(name="description")
	private String description;
	@Column(name="component")
	private String component;
	
	
	
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCycleName() {
		return cycleName;
	}
	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public CycleFeatures(String cycleName, String description) {
		super();
		this.cycleName = cycleName;
		this.description = description;
	}
	@Override
	public String toString() {
		return "CycleFeature [id=" + id + ", cycleName=" + cycleName + ", description=" + description + "]";
	}
	
	

}
