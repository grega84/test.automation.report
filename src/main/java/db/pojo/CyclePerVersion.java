package db.pojo;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@javax.persistence.Entity
@javax.persistence.Table(name = "cycle_per_version")
public class CyclePerVersion {
	public CyclePerVersion() {}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_row")
	private int idRow;
	@Column(name="id_version")
	private String idVersion;
	@Column(name="id_cycle")
	private String idCycle;
	public int getIdRow() {
		return idRow;
	}
	public void setIdRow(int idRow) {
		this.idRow = idRow;
	}
	public String getIdVersion() {
		return idVersion;
	}
	public void setIdVersion(String idVersion) {
		this.idVersion = idVersion;
	}
	public String getIdCycle() {
		return idCycle;
	}
	public void setIdCycle(String idCycle) {
		this.idCycle = idCycle;
	}
	public CyclePerVersion(String idVersion, String idCycle) {
		super();
		this.idVersion = idVersion;
		this.idCycle = idCycle;
	}
	
	

}
