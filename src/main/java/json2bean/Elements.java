/* Copyright 2020 freecodeformat.com */
package json2bean;
import java.util.List;

/* Time: 2020-02-19 12:50:0 @author freecodeformat.com @website http://www.freecodeformat.com/json2javabean.php */
public class Elements {
	private List<Comments> comments;
	private List<Before> before;
    private int line;
    private String name;
    private String description;
    private String id;
    private List<After> after;
    private String type;
    private String keyword;
    private List<Steps> steps;
    private List<Tags> tags;
    public List<Tags> getTags() {
		return tags;
	}
	public void setTags(List<Tags> tags) {
		this.tags = tags;
	}
	public void setBefore(List<Before> before) {
         this.before = before;
     }
     public List<Before> getBefore() {
         return before;
     }

    public void setLine(int line) {
         this.line = line;
     }
     public int getLine() {
         return line;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public List<Comments> getComments() {
		return comments;
	}
	public void setComments(List<Comments> comments) {
		this.comments = comments;
	}
	public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setAfter(List<After> after) {
         this.after = after;
     }
     public List<After> getAfter() {
         return after;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setKeyword(String keyword) {
         this.keyword = keyword;
     }
     public String getKeyword() {
         return keyword;
     }

    public void setSteps(List<Steps> steps) {
         this.steps = steps;
     }
     public List<Steps> getSteps() {
         return steps;
     }

}