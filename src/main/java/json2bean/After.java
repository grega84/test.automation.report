/* Copyright 2020 freecodeformat.com */
package json2bean;

/* Time: 2020-02-19 12:50:0 @author freecodeformat.com @website http://www.freecodeformat.com/json2javabean.php */
public class After {

    private Result result;
    private Match match;
    public void setResult(Result result) {
         this.result = result;
     }
     public Result getResult() {
         return result;
     }

    public void setMatch(Match match) {
         this.match = match;
     }
     public Match getMatch() {
         return match;
     }

}