/* Copyright 2020 freecodeformat.com */
package json2bean;
import java.util.List;

/* Time: 2020-02-19 12:50:0 @author freecodeformat.com @website http://www.freecodeformat.com/json2javabean.php */
public class Rows {

    private List<String> cells;
    private int line;
    public void setCells(List<String> cells) {
         this.cells = cells;
     }
     public List<String> getCells() {
         return cells;
     }

    public void setLine(int line) {
         this.line = line;
     }
     public int getLine() {
         return line;
     }

}