/* Copyright 2020 freecodeformat.com */
package json2bean;

/* Time: 2020-02-19 12:50:0 @author freecodeformat.com @website http://www.freecodeformat.com/json2javabean.php */
public class Result {

    private long duration;
    private String status;
    private String error_message;
    public void setDuration(long duration) {
         this.duration = duration;
     }
     public long getDuration() {
         return duration;
     }

    public void setStatus(String status) {
         this.status = status;
     }
     public String getStatus() {
         return status;
     }
	public String getError_message() {
		return error_message;
	}
	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

}