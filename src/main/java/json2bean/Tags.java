package json2bean;

public class Tags {

    private int line;
    private String name;
    public void setLine(int line) {
         this.line = line;
     }
     public int getLine() {
         return line;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

}