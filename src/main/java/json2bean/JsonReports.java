/* Copyright 2020 freecodeformat.com */
package json2bean;
import java.util.List;

/* Time: 2020-02-19 12:50:0 @author freecodeformat.com @website http://www.freecodeformat.com/json2javabean.php */
public class JsonReports {

    List<JsonReport> jsonReportList;

	public List<JsonReport> getJsonReportList() {
		return jsonReportList;
	}

	public void setJsonReportList(List<JsonReport> jsonReportList) {
		this.jsonReportList = jsonReportList;
	}
    

}