/* Copyright 2020 freecodeformat.com */
package json2bean;

import java.util.List;

/* Time: 2020-02-19 12:50:0 @author freecodeformat.com @website http://www.freecodeformat.com/json2javabean.php */
public class Match {
	private List<Argument> arguments;
    public List<Argument> getArguments() {
		return arguments;
	}
	public void setArguments(List<Argument> arguments) {
		this.arguments = arguments;
	}
	private String location;
    public void setLocation(String location) {
         this.location = location;
     }
     public String getLocation() {
         return location;
     }

}