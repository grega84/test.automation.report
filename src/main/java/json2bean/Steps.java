/* Copyright 2020 freecodeformat.com */
package json2bean;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
/* Time: 2020-02-19 12:50:0 @author freecodeformat.com @website http://www.freecodeformat.com/json2javabean.php */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Steps {

    private Result result;
    private int line;
    private String name;
    private Match match;
    @JsonProperty("matchedColumns")
    private List<Integer> matchedcolumns;
    private List<Rows> rows;
    private String keyword;
    private List<String> output;
    private List<String> embenddings;
    
    public List<String> getEmbenddings() {
		return embenddings;
	}
	public void setEmbenddings(List<String> embenddings) {
		this.embenddings = embenddings;
	}
	public void setResult(Result result) {
         this.result = result;
     }
     public Result getResult() {
         return result;
     }

    public void setLine(int line) {
         this.line = line;
     }
     public int getLine() {
         return line;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setMatch(Match match) {
         this.match = match;
     }
     public Match getMatch() {
         return match;
     }

    public void setMatchedcolumns(List<Integer> matchedcolumns) {
         this.matchedcolumns = matchedcolumns;
     }
     public List<Integer> getMatchedcolumns() {
         return matchedcolumns;
     }

    public void setRows(List<Rows> rows) {
         this.rows = rows;
     }
     public List<Rows> getRows() {
         return rows;
     }

    public void setKeyword(String keyword) {
         this.keyword = keyword;
     }
     public String getKeyword() {
         return keyword;
     }
	public List<String> getOutput() {
		return output;
	}
	public void setOutput(List<String> output) {
		this.output = output;
	}

}