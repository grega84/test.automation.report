/* Copyright 2020 freecodeformat.com */
package json2bean;
import java.util.List;

/* Time: 2020-02-19 12:50:0 @author freecodeformat.com @website http://www.freecodeformat.com/json2javabean.php */
public class JsonReport {

    private int line;
    private List<Elements> elements;
    private String name;
    private String description;
    private String id;
    private String keyword;
    private String uri;
    private List<Tags> tags;
    
    public List<Tags> getTags() {
		return tags;
	}
	public void setTags(List<Tags> tags) {
		this.tags = tags;
	}
	public void setLine(int line) {
         this.line = line;
     }
     public int getLine() {
         return line;
     }

    public void setElements(List<Elements> elements) {
         this.elements = elements;
     }
     public List<Elements> getElements() {
         return elements;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setDescription(String description) {
         this.description = description;
     }
     public String getDescription() {
         return description;
     }

    public void setId(String id) {
         this.id = id;
     }
     public String getId() {
         return id;
     }

    public void setKeyword(String keyword) {
         this.keyword = keyword;
     }
     public String getKeyword() {
         return keyword;
     }

    public void setUri(String uri) {
         this.uri = uri;
     }
     public String getUri() {
         return uri;
     }

}