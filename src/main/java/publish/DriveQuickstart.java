package publish;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.mysql.cj.x.protobuf.MysqlxNotice.Frame.Scope;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DriveQuickstart {
    private static final String APPLICATION_NAME = "Google Drive API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static List<String> SCOPES = new ArrayList<String>();
    
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
    	SCOPES.addAll(DriveScopes.all());
    	
        InputStream in = DriveQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("online")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("ciaoprisma");
    }

    public static void main(String... args) throws Exception {
    	
    	
    	
    	
    	
    	        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();

//        // Print the names and IDs for up to 10 files.
//        FileList result = service.files().list()
//                .setPageSize(80)
//                .setFields("nextPageToken, files(id, name)")
//                .execute();
//        List<File> files = result.getFiles();
//        if (files == null || files.isEmpty()) {
//            System.out.println("No files found.");
//        } else {
//            System.out.println("Files:");
//            for (File file : files) {
//                System.out.printf("%s (%s)\n", file.getName(), file.getId());
//            }
//        }
//        
        LinkedList<String> folders = new LinkedList<String>();
        folders.add("1B8I9RRYLInhbqDEcVwj8FvenjbOxw3M2");
//        System.out.println(createFolderIn(service, "prova", folders));
//        String currf="1Ns60BgrqLJbeqW4ptM9bQlueIivOJZdl"
        DriveQuickstart dq = new DriveQuickstart();
        java.io.File fileStart = new java.io.File("C:\\Users\\User\\Downloads\\Nuova cartella\\cucumber-html-reports");
    	dq.addToDrive(service,fileStart,folders );
//    	
    }
    public void uploadFile(Drive service,List<String> folders,java.io.File files) throws IOException {
//    	String folder= "1B8I9RRYLInhbqDEcVwj8FvenjbOxw3M2";
        File fileMetadata = new File();
    	fileMetadata.setName(files.getName());
    	fileMetadata.setParents(folders);
    	java.io.File filePath = new java.io.File(files.toString());
    	FileContent mediaContent = new FileContent(null, filePath);
    	File file = service.files().create(fileMetadata, mediaContent)
    	    .setFields("id, parents")
    	    .execute();
    	System.out.println("File ID: " + file.getId());
    }
    
    public static String createFolderIn(Drive service,java.io.File files, List<String> folders) throws Exception {
    	File fileMetadata = new File();
    	fileMetadata.setName(files.getName());
    	fileMetadata.setParents(folders);
    	fileMetadata.setMimeType("application/vnd.google-apps.folder");
    	File file = service.files().create(fileMetadata)
    	    .setFields("id, parents")
    	    .execute();
    	return file.getId();
    }
    
    public void test(Drive service,java.io.File files,List<String> folders) throws Exception {
//    	createFolder(service,)
//    	java.io.File file = new java.io.File("C:\\prova");
    	String nextFolder="";
		if(files.isDirectory())
		{
			java.io.File[] filesInDir = files.listFiles();
			Arrays.sort(filesInDir);
			for(java.io.File f : filesInDir)
			{
				String prefix = "";

				if(f.isFile()) {
					uploadFile(service, folders, f);
					prefix = "[f] ";
				}
				else if(f.isDirectory()) {
					nextFolder=createFolderIn(service,f,folders);
					folders.add(nextFolder);
					test(service,f,folders);
					prefix = "[d] ";
					
				}
				
//				System.out.println(prefix + f.toString());
//				System.out.println(f.getName());
			}
			folders.remove(folders.size()-1);
		}
//    	C:\Users\User\Downloads\Nuova cartella\cucumber-html-reports
    }
    
    public void addToDrive(Drive service,java.io.File file,LinkedList<String> folders) throws Exception
    {
    	if(file.isDirectory())
    	{
    		String nextFolder=createFolderIn(service,file,folders);
    		LinkedList<String> newFolder= new LinkedList<String>();
    		newFolder.add(nextFolder);
//			folders.add(nextFolder);
			
			for(java.io.File subfile : file.listFiles())
			{
				addToDrive(service, subfile, newFolder);
			}
			
//			folders.removeLast();
    	}
    	else
    	{
    		uploadFile(service, folders, file);
    	}
    }
}