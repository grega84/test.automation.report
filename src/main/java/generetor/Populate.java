package generetor;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Populate {
	
	public void writeObjects(List<Object> Objects) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("conf\\hibernate.cfg.xml").build();  
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();  

		SessionFactory factory = meta.getSessionFactoryBuilder().build();  
		Session session = factory.openSession();  
		Transaction t = session.beginTransaction();   
		for (Object object : Objects) {
			session.save(object);  

		}
		t.commit();
		factory.close();  
		session.close();
	}
}
